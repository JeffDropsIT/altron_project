/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.altron;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 *
 * @author jeffdropsit
 */
@MessageDriven(mappedName = "exampleMDB", activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class MessageBean implements MessageListener {

    @Resource
    private TimerService timerService;

    public MessageBean() {
    }

    @Timeout
    protected void process(final Timer timer) {
        Logger.getLogger(MessageBean.class.getName()).log(Level.INFO,
                "MESSAGE after 10 seconds: Message received: {0}",
                timer.getInfo());
    }

    @Override
    public void onMessage(final Message message) {
        Logger.getLogger(MessageBean.class.getName()).log(Level.INFO, "On message event received message: {0}", message.toString());
        try {
            if (message instanceof TextMessage) {
                timerService.createSingleActionTimer(10000, new TimerConfig(message.getBody(String.class), true));
            }

        } catch (JMSException e) {
            Logger.getLogger(MessageBean.class.getName()).log(Level.SEVERE, "An Error occured while reading message: ", e);
        }
    }

}
