<%-- 
    Document   : index
    Created on : 28 Nov 2020, 10:25:09 AM
    Author     : jeffdropsit
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Message Example</title>
    </head>
    <body>
        <h1>Messaging form</h1>
        
        <form action="MessagingServlet">
            Enter message here: <input type="text" name="message">
            <br/>
            <input type="submit" name="send">
        </form>
    </body>
</html>
