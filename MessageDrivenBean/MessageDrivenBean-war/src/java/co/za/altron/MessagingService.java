/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.altron;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.faces.bean.RequestScoped;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 *
 * @author jeffdropsit
 */
@Stateless
public class MessagingService {

    @Resource(mappedName = "exampleMDB")
    private Queue exampleMDB;
    @Resource(mappedName = "queue/test")
    private ConnectionFactory connectionFactory;

    public MessagingService() {
    }

    private Message createJMSMessageForexampleMDB(final Session session, final Object messageData) throws JMSException {
        final TextMessage tm = session.createTextMessage();
        if (messageData instanceof String) {
            tm.setText((String) messageData);
        } else {
            tm.setText(messageData.toString());
        }
        return tm;
    }

    public void sendJMSMessageToExampleMDB(final Object messageData) throws JMSException {
        Connection connection = null;
        Session session = null;
        try {
            connection = connectionFactory.createConnection();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer(exampleMDB);
            messageProducer.send(createJMSMessageForexampleMDB(session, messageData));
        } catch (JMSException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Failed to send message", e);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException e) {
                    Logger.getLogger(this.getClass().getName()).log(Level.WARNING, "Cannot close session", e);
                }
            }
            if (connection != null) {
                connection.close();
            }
        }
    }
}
